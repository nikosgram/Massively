/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.massively;

import lombok.Getter;
import me.nikosgram.massively.api.entity.Mobile;
import me.nikosgram.massively.api.rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.SmallFireball;
import org.bukkit.entity.WitherSkull;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings("unused")
public class MageRank implements Rank, Listener {
    private final MassivelyBukkit massively;
    @Getter
    private final String name = "Mage";
    @Getter
    private final String id = "mage";
    @Getter
    @SuppressWarnings("all")
    private final Collection<Rank> parents = new ArrayList<>();
    @Getter
    private final double health = 20;
    @Getter
    private final double startMana = 100;
    @Getter
    private final double appendManaPerLevel = 25;
    @Getter
    private final double appendManaPerRefresh = 5;
    private boolean registered = false;

    protected MageRank(MassivelyBukkit massively) {
        this.massively = massively;
        if (massively.rankManager.contains("human")) {
            parents.add(massively.rankManager.getRank("human").get());
        }
    }

    @Override
    public void registerEvents() {
        if (registered) {
            throw new RuntimeException();
        }
        massively.getServer().getPluginManager().registerEvents(this, massively);
        registered = true;
    }

    @EventHandler
    public void shotWitherSkull(PlayerInteractEvent event) {
        if (event.getAction().equals(Action.LEFT_CLICK_AIR) || event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
            if (event.getItem() != null) {
                switch (event.getItem().getType()) {
                    case BLAZE_ROD:
                        if (event.getItem().getItemMeta().getLore() != null) {
                            if (event.getItem().getItemMeta().getLore().contains("wither_stick")) {
                                Mobile mobile = massively.mobileManager.getMobile(event.getPlayer().getUniqueId()).get();
                                if (mobile.hasRank(id)) {
                                    double neededMana = MassivelyUtils.estimateNeededMana(mobile, 20);
                                    if (mobile.getManaData().hasMana(neededMana)) {
                                        event.getPlayer().launchProjectile(WitherSkull.class);
                                        if (event.getPlayer().isSneaking()) {
                                            if (mobile.getExperiencesData().getLevel() > 10) {
                                                Bukkit.getScheduler().scheduleSyncDelayedTask(massively, () -> event.getPlayer().launchProjectile(WitherSkull.class), 10);
                                            }
                                        }
                                        mobile.getManaData().removeMana(neededMana);
                                    } else {
                                        mobile.sendMessage("You don't have enough mana. Needs " + neededMana + " and you have " + mobile.getManaData().getMana() + '/' + mobile.getManaData().getMaxMana());
                                    }
                                    event.setCancelled(true);
                                }
                            }
                        }
                        break;
                }
            } else if (event.getPlayer().isSneaking()) {
                if (event.getAction().equals(Action.LEFT_CLICK_AIR)) {
                    Mobile mobile = massively.mobileManager.getMobile(event.getPlayer().getUniqueId()).get();
                    if (mobile.hasRank(id)) {
                        double neededMana = MassivelyUtils.estimateNeededMana(mobile, 5);
                        if (mobile.getManaData().hasMana(neededMana)) {
                            event.getPlayer().launchProjectile(SmallFireball.class);
                            mobile.getManaData().removeMana(neededMana);
                        } else {
                            mobile.sendMessage("You don't have enough mana. Needs " + neededMana + " and you have " + mobile.getManaData().getMana() + '/' + mobile.getManaData().getMaxMana());
                        }
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onHitEntity(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof WitherSkull) {
            WitherSkull skull = (WitherSkull) event.getDamager();
            if (skull.getShooter() instanceof Player) {
                if (event.getEntity().getUniqueId().equals(((Player) skull.getShooter()).getUniqueId())) {
                    event.setCancelled(true);
                } else {
                    double damage = event.getDamage();
                    Mobile mobile = massively.mobileManager.getMobile(((Player) skull.getShooter()).getUniqueId()).get();
                    if (mobile.hasRank(id)) {
                        event.setDamage(event.getDamage() + (2.25 * mobile.getExperiencesData().getLevel()));
                    }
                    mobile.sendMessage(event.getFinalDamage() + "/" + damage);
                }
            }
        } else if (event.getDamager() instanceof SmallFireball) {
            SmallFireball fireball = (SmallFireball) event.getDamager();
            if (fireball.getShooter() instanceof Player) {
                if (event.getEntity().getUniqueId().equals(((Player) fireball.getShooter()).getUniqueId())) {
                    event.setCancelled(true);
                } else {
                    double damage = event.getDamage();
                    Mobile mobile = massively.mobileManager.getMobile(((Player) fireball.getShooter()).getUniqueId()).get();
                    if (mobile.hasRank(id)) {
                        event.setDamage(event.getDamage() + (0.25 * mobile.getExperiencesData().getLevel()));
                    }
                    mobile.sendMessage(event.getFinalDamage() + "/" + damage);
                }
            }
        } else if (event.getDamager() instanceof Arrow) {
            Arrow arrow = (Arrow) event.getDamager();
            if (arrow.getShooter() instanceof Player) {
                double damage = event.getDamage();
                Mobile mobile = massively.mobileManager.getMobile(((Player) arrow.getShooter()).getUniqueId()).get();
                if (mobile.hasRank(id)) {
                    event.setDamage((event.getDamage() / 100) * 20); //-80%
                }
                mobile.sendMessage(event.getFinalDamage() + "/" + damage);
            }
        } else if (event.getDamager() instanceof Player) {
            Mobile mobile = massively.mobileManager.getMobile(event.getDamager().getUniqueId()).get();
            double damage = event.getDamage();
            if (mobile.hasRank(id)) {
                event.setDamage((event.getDamage() / 100) * 20); //-80%
            }
            mobile.sendMessage(event.getFinalDamage() + "/" + damage);
        }
    }

    @EventHandler
    public void onKillEntity(EntityDeathEvent event) {
        if (event.getEntity().getKiller() != null) {
            Mobile mobile = massively.mobileManager.getMobile(event.getEntity().getKiller().getUniqueId()).get();
            if (mobile.hasRank(id)) {
                mobile.getHealthData().appendHealth((event.getEntity().getMaxHealth() / 100) * (2.5 * mobile.getExperiencesData().getLevel()));
                switch (event.getEntityType()) {
                    case PLAYER:
                        mobile.getExperiencesData().appendExperiences(22.5);
                        break;
                    default:
                        EntityCategory category = EntityCategory.getCategory(event.getEntityType());
                        switch (category) {
                            case Boss:
                                mobile.getManaData().appendMana(20);
                                break;
                            case Unused:
                                mobile.getManaData().appendMana(19.25);
                                break;
                            case Utility:
                                mobile.getManaData().appendMana(2.5);
                                break;
                            case Passive:
                            case Tamable:
                                mobile.getManaData().appendMana(1.25);
                                break;
                            case Hostile:
                                mobile.getManaData().appendMana(10.125);
                                break;
                            case Neutral:
                                mobile.getManaData().appendMana(12.625);
                                break;
                        }
                }
            }
        }
    }
}
