/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.massively;

import com.sk89q.intake.argument.ArgumentException;
import com.sk89q.intake.argument.ArgumentParseException;
import com.sk89q.intake.argument.CommandArgs;
import com.sk89q.intake.parametric.ProvisionException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import me.nikosgram.massively.api.RankManager;
import me.nikosgram.massively.api.rank.Rank;

import javax.annotation.Nullable;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class MassivelyRankManager implements RankManager {
    protected final Map<String, Rank> ranks = new HashMap<>();
    private final MassivelyBukkit massively;

    @Override
    public Optional<Rank> getRank(String target) {
        return Optional.ofNullable(contains(target) ? ranks.get(target) : null);
    }

    @Override
    public boolean registerRank(Rank rank) {
        if (!ranks.containsKey(Objects.requireNonNull(rank).getId())) {
            ranks.put(rank.getId(), rank);
            return true;
        }
        return false;
    }

    @Override
    public boolean contains(String target) {
        return ranks.containsKey(target);
    }

    @Override
    public Iterator<Rank> iterator() {
        return ranks.values().iterator();
    }

    @Override
    public boolean isProvided() {
        return false;
    }

    @Nullable
    @Override
    public Rank get(CommandArgs arguments, List<? extends Annotation> modifiers) throws ArgumentException, ProvisionException {
        String name = arguments.next();
        Optional<Rank> rank = getRank(name);
        if (rank.isPresent()) {
            return rank.get();
        } else {
            throw new ArgumentParseException(String.format("I can't find the Rank with name '%s'.", name));
        }
    }

    @Override
    public List<String> getSuggestions(String prefix) {
        List<String> returned = new ArrayList<>();
        System.out.println(prefix);
        System.out.println(this);
        this.forEach(rank -> {
            if (rank.getName().startsWith(prefix)) {
                System.out.println(rank.getName());
                returned.add(rank.getName());
            }
        });
        return returned;
    }
}
