/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.massively.database;

import lombok.Getter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;

public class MySQLDatabaseDriver implements DatabaseDriver {
    @Getter
    private final String name = "MySQL";
    private final String username;
    private final String database;
    private final String password;
    private final String hostname;
    private final Integer port;
    private Connection connection = null;

    public MySQLDatabaseDriver(String username, String database, String password, String hostname, Integer port) {
        this.username = username;
        this.database = database;
        this.password = password;
        this.hostname = hostname;
        this.port = port;
    }

    @Override
    public DatabaseDriver openConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:mysql://" +
                            hostname +
                            ":" +
                            port +
                            "/" +
                            database,
                    username,
                    password
            );
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return this;
    }

    @Override
    public Boolean checkConnection() {
        if (connection == null) {
            return false;
        }
        try {
            return !connection.isClosed();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void closeConnection() {
        if (checkConnection()) {
            try {
                connection.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public Optional<Connection> getConnection() {
        return Optional.ofNullable(connection);
    }
}
