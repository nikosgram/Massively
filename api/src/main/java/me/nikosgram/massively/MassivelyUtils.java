/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.massively;

import me.nikosgram.massively.api.entity.Mobile;
import me.nikosgram.massively.api.rank.Rank;

import java.util.Optional;

@SuppressWarnings("unused")
public final class MassivelyUtils {
    public static int estimateLevel(double experiences) {
        return (int) Math.round(Math.floor(25 + Math.sqrt(625 + 100 * experiences)) / 50);
    }

    public static double estimateMaxMana(Mobile mobile) {
        double maxMana = 0.0;
        Optional<Rank> rank = mobile.getRank();
        if (rank.isPresent()) {
            maxMana = rank.get().getStartMana() + (rank.get().getAppendManaPerLevel() * (mobile.getExperiencesData().getLevel() - 1));
        }
        return maxMana;
    }

    public static double estimateNeededMana(Mobile mobile, double mana) {
        return mana + (2 * (mobile.getExperiencesData().getLevel() - 1));
    }
}
