/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.massively;

import lombok.Getter;
import me.nikosgram.massively.api.entity.Mobile;
import me.nikosgram.massively.api.rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings("unused")
public class ElfRank implements Rank, Listener {
    private final MassivelyBukkit massively;
    @Getter
    private final String name = "Elf";
    @Getter
    private final String id = "elf";
    @Getter
    private final Collection<Rank> parents = new ArrayList<>();
    @Getter
    private final double health = 20;
    @Getter
    private final double startMana = 0;
    @Getter
    private final double appendManaPerLevel = 0;
    @Getter
    private final double appendManaPerRefresh = 0;
    private boolean registered = false;

    protected ElfRank(MassivelyBukkit massively) {
        this.massively = massively;
    }

    @Override
    public void registerEvents() {
        if (registered) {
            throw new RuntimeException();
        }
        massively.getServer().getPluginManager().registerEvents(this, massively);
        registered = true;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(EntitySpawnEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            Bukkit.getScheduler().scheduleSyncDelayedTask(massively, () -> {
                if (player.isOnline()) {
                    if (massively.mobileManager.contains(player.getUniqueId())) {
                        Mobile mobile = massively.mobileManager.getMobile(player.getUniqueId()).get();
                        if (mobile.hasRank(id)) {
                            player.setWalkSpeed((0.5F / 100F) * 100.25F);
                        }
                    }
                }
            }, 20);
        }
    }
}