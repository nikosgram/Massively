/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.massively;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.io.Files;
import com.sk89q.intake.CommandException;
import com.sk89q.intake.Intake;
import com.sk89q.intake.InvalidUsageException;
import com.sk89q.intake.InvocationCommandException;
import com.sk89q.intake.argument.Namespace;
import com.sk89q.intake.dispatcher.Dispatcher;
import com.sk89q.intake.fluent.CommandGraph;
import com.sk89q.intake.parametric.Injector;
import com.sk89q.intake.parametric.ParametricBuilder;
import com.sk89q.intake.parametric.provider.PrimitivesModule;
import com.sk89q.intake.util.auth.AuthorizationException;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import me.nikosgram.massively.api.MassivelyPlugin;
import me.nikosgram.massively.api.Platform;
import me.nikosgram.massively.api.chat.MessageColor;
import me.nikosgram.massively.api.entity.CommandExecutor;
import me.nikosgram.massively.api.rank.Rank;
import me.nikosgram.massively.database.DatabaseConnector;
import me.nikosgram.massively.database.MySQLDatabaseDriver;
import me.nikosgram.massively.database.SQLiteDatabaseDriver;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.UUID;

@SuppressWarnings("unused")
public class MassivelyBukkit extends JavaPlugin implements MassivelyPlugin, Listener {
    protected final HandlerList handlers = new HandlerList(this);
    protected final ConsoleExecutor consoleExecutor;
    @Getter
    protected final MassivelyRankManager rankManager = new MassivelyRankManager(this);
    @Getter
    private final Platform platform = Platform.Spigot;
    @Getter
    private final Properties properties;
    protected Dispatcher dispatcher;
    @Getter
    protected MassivelyMobileManager mobileManager = null;
    @Getter
    protected DatabaseConnector connector = null;

    {
        rankManager.registerRank(new HumanRank(this));
        rankManager.registerRank(new MageRank(this));
        rankManager.registerRank(new ElfRank(this));
        rankManager.registerRank(new ArcherRank(this));
    }

    public MassivelyBukkit() {
        Long delay = System.currentTimeMillis();
        InputStream stream = this.getClass().getResourceAsStream("/.properties");
        properties = new Properties();
        try {
            properties.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        consoleExecutor = new ConsoleExecutor(this, Bukkit.getConsoleSender());

        Massively.invoke(this);

        getLogger().info("Takes " + (System.currentTimeMillis() - delay) + "ms to _construction.");
    }

    @Override
    public void onLoad() {
        Long delay = System.currentTimeMillis();
        getLogger().info("Checking for the installed plugins...");
        if (getServer().getPluginManager().getPlugin("Citizens") != null) {
            registerHandler(new CitizensHandler(this));
        }
        if (getServer().getPluginManager().getPlugin("WorldGuard") != null) {
            registerHandler(new WorldGuardHandler(this));
        }

        getLogger().info("Loading the configuration file.");
        saveDefaultConfig();
        getConfig().options().copyDefaults(true);

        int version = getConfig().getInt("version");

        switch (version) {
            case 1:
                break;
            default:
                getLogger().warning("You have unsupported configuration version.");
                Path newPath = Paths.get(getDataFolder().toString(), "config-old-v" + version + ".yml");
                getLogger().warning("Your unsupported configuration file moved to '" + newPath.toString() + "'.");
                Path path = Paths.get(getDataFolder().toString(), "config.yml");
                try {
                    Files.move(path.toFile(), newPath.toFile());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                saveDefaultConfig();
                getConfig().options().copyDefaults(true);
                break;
        }

        handlers.onLoad();
        getLogger().info("Takes " + (System.currentTimeMillis() - delay) + "ms to loading.");
    }

    @Override
    public void onEnable() {
        Long delay = System.currentTimeMillis();
        getLogger().info("Opening the SQL connection.");
        if (getConfig().getString("database.type").equalsIgnoreCase("sqlite")) {
            connector = new DatabaseConnector(new SQLiteDatabaseDriver(
                    Paths.get(getConfig().getString("database.host"))
            )).openConnection();
        } else {
            connector = new DatabaseConnector(new MySQLDatabaseDriver(
                    getConfig().getString("database.user"),
                    getConfig().getString("database.data"),
                    getConfig().getString("database.pass"),
                    getConfig().getString("database.host"),
                    getConfig().getInt("database.port")
            )).openConnection();
        }
        getLogger().info("The SQL connected.");

        getServer().getPluginManager().registerEvents(this, this);

        mobileManager = new MassivelyMobileManager(this);

        rankManager.iterator().forEachRemaining(Rank::registerEvents);

        Injector injector = Intake.createInjector();
        injector.install(new PrimitivesModule());
        injector.install(new MassivelyModule());

        ParametricBuilder builder = new ParametricBuilder(injector);
        builder.setAuthorizer(new MassivelyAuthorizer());

        dispatcher = new CommandGraph()
                .builder(builder)
                .commands()
                .registerMethods(new MassivelyCommands())
                .graph()
                .getDispatcher();

        {
            ItemStack wolfArmy = new ItemStack(Material.BONE, 1);
            ItemMeta meta = wolfArmy.getItemMeta();
            meta.setDisplayName("Wolf Army");
            List<String> lore = new ArrayList<>();
            lore.add("wolf_army");
            meta.setLore(lore);
            wolfArmy.setItemMeta(meta);

            ShapedRecipe wolfArmyRecipe = new ShapedRecipe(wolfArmy);
            wolfArmyRecipe.shape(
                    " O ",
                    "OIO",
                    " O "
            );

            wolfArmyRecipe.setIngredient('O', Material.BLAZE_POWDER);
            wolfArmyRecipe.setIngredient('I', Material.BONE);

            getServer().addRecipe(wolfArmyRecipe);
        }
        {
            ItemStack invincibleStar = new ItemStack(Material.FIREWORK_CHARGE, 1);
            ItemMeta meta = invincibleStar.getItemMeta();
            meta.setDisplayName("Invincible Star");
            List<String> lore = new ArrayList<>();
            lore.add("invincible_star");
            meta.setLore(lore);
            invincibleStar.setItemMeta(meta);

            ShapedRecipe wolfArmyRecipe = new ShapedRecipe(invincibleStar);
            wolfArmyRecipe.shape(
                    "   ",
                    "OIO",
                    "   "
            );

            wolfArmyRecipe.setIngredient('O', Material.BLAZE_POWDER);
            wolfArmyRecipe.setIngredient('I', Material.FIREWORK_CHARGE);

            getServer().addRecipe(wolfArmyRecipe);
        }
        {
            ItemStack witherStick = new ItemStack(Material.BLAZE_ROD, 1);
            ItemMeta meta = witherStick.getItemMeta();
            meta.setDisplayName("Wither Stick");
            List<String> lore = new ArrayList<>();
            lore.add("wither_stick");
            meta.setLore(lore);
            witherStick.setItemMeta(meta);

            ShapedRecipe witherStickRecipe = new ShapedRecipe(witherStick);
            witherStickRecipe.shape(
                    " OS",
                    "OIO",
                    "IO "
            );

            witherStickRecipe.setIngredient('O', Material.BLAZE_POWDER);
            witherStickRecipe.setIngredient('S', Material.NETHER_STAR);
            witherStickRecipe.setIngredient('I', Material.STICK);

            getServer().addRecipe(witherStickRecipe);
        }

        handlers.onEnable();
        getLogger().info("Takes " + (System.currentTimeMillis() - delay) + "ms to enabling.");
    }

    @Override
    public void onDisable() {
        Long delay = System.currentTimeMillis();
        handlers.onDisable();
        if (!rankManager.ranks.isEmpty()) {
            rankManager.ranks.clear();
        }
        if (connector.checkConnection()) {
            getLogger().info("Closing the SQL connection...");
            connector.closeConnection();
        }
        getLogger().info("Takes " + (System.currentTimeMillis() - delay) + "ms to disabling.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (handlers.onCommand(sender, command, label, args)) {
            return true;
        }
        Namespace namespace = new Namespace();
        if (sender instanceof Player) {
            namespace.put(CommandExecutor.class, mobileManager.getMobile(((Player) sender).getUniqueId()).get());
        } else {
            namespace.put(CommandExecutor.class, consoleExecutor);
        }
        try {
            return dispatcher.call(command.getName() + ' ' + Joiner.on(' ').join(args), namespace, ImmutableList.of());
        } catch (InvalidUsageException e) {
            sender.sendMessage(MessageColor.RED + "Usage: " + e.getSimpleUsageString("/"));
            return true;
        } catch (InvocationCommandException e) {
            sender.sendMessage(MessageColor.RED + e.getLocalizedMessage());
            e.printStackTrace();
        } catch (CommandException e) {
            sender.sendMessage(MessageColor.RED + e.getLocalizedMessage());
            return true;
        } catch (AuthorizationException e) {
            sender.sendMessage(command.getPermissionMessage());
            return true;
        }
        return super.onCommand(sender, command, label, args);
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        List<String> returned;
        if ((returned = handlers.onTabComplete(sender, command, alias, args)) != null) {
            return returned;
        }
        Namespace namespace = new Namespace();
        if (sender instanceof Player) {
            namespace.put(CommandExecutor.class, mobileManager.getMobile(((Player) sender).getUniqueId()).get());
        } else {
            namespace.put(CommandExecutor.class, consoleExecutor);
        }
        try {
            return dispatcher.getSuggestions(command.getName() + ' ' + Joiner.on(' ').join(args), namespace);
        } catch (CommandException e) {
            sender.sendMessage(MessageColor.RED + e.getLocalizedMessage());
            return ImmutableList.of();
        }
    }

    public boolean registerHandler(Handler handler) {
        return handlers.registerHandler(handler);
    }

    @Override
    public String getVersion() {
        return properties.getProperty("version");
    }

    @Override
    public String getVersionName() {
        return properties.getProperty("version-name");
    }

    @Override
    public int getProtocol() {
        return (int) properties.get("protocol");
    }

    @EventHandler
    public void onChunkUnload(ChunkUnloadEvent event) {
        for (Entity entity : event.getChunk().getEntities()) {
            if (entity instanceof LivingEntity) {
                LivingEntity living = (LivingEntity) entity;
                living.setHealth(0);
            } else if (entity instanceof Item) {
                Item item = (Item) entity;
                item.remove();
            } else if (entity instanceof Projectile) {
                Projectile projectile = (Projectile) entity;
                projectile.remove();
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onEntityHitEntity(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            addKiller(event.getEntity(), (Player) event.getDamager());
        } else if (event.getDamager() instanceof Projectile) {
            if (((Projectile) event.getDamager()).getShooter() instanceof Player) {
                addKiller(event.getEntity(), (Player) ((Projectile) event.getDamager()).getShooter());
            }
        } else if (event.getDamager() instanceof TNTPrimed) {
            TNTPrimed tnt = (TNTPrimed) event.getDamager();
            if (tnt.getSource() instanceof Player) {
                addKiller(event.getEntity(), (Player) tnt.getSource());
            }
        }
    }

    @SuppressWarnings("unchecked")
    protected List<Player> getKillers(Entity entity) {
        if (entity.hasMetadata("killers")) {
            ImmutableList.Builder<Player> players = ImmutableList.builder();
            entity.getMetadata("killers").forEach(value -> ((ArrayList<UUID>) value.value()).stream().map(
                    Bukkit::getPlayer
            ).filter(player -> player != null).forEach(players::add));
            return players.build();
        }
        return ImmutableList.of();
    }

    @SuppressWarnings("unchecked")
    protected void addKiller(Entity entity, Player player) {
        if (!entity.hasMetadata("killers")) {
            entity.setMetadata("killers", new FixedMetadataValue(this, new ArrayList<UUID>()));
        }
        entity.getMetadata("killers").forEach(value -> {
            ((ArrayList<UUID>) value.value()).add(player.getUniqueId());
            Bukkit.getScheduler().scheduleSyncDelayedTask(this, () -> {
                if (!entity.isDead()) {
                    ((List<UUID>) entity.getMetadata("killers").get(0).value()).remove(player.getUniqueId());
                    System.out.println("add " + player.getName());
                }
            }, 60);
        });
    }

    @RequiredArgsConstructor(access = AccessLevel.PROTECTED)
    protected static class HandlerList extends Handler implements Iterable<Handler> {
        private final Map<String, Handler> handlers = new HashMap<>();
        private final MassivelyBukkit massively;
        @Getter
        private String name = "HandlerList";

        @Override
        public void onLoad() {
            massively.getLogger().info("Loading the handlers...");
            this.forEach(handler -> {
                massively.getLogger().info("Loading the handler with name '" + handler.getName() + "'.");
                handler.onDisable();
            });
        }

        @Override
        public void onEnable() {
            massively.getLogger().info("Enabling the handlers...");
            this.forEach(handler -> {
                massively.getLogger().info("Enabling the handler with name '" + handler.getName() + "'.");
                handler.onDisable();
            });
        }

        @Override
        public void onDisable() {
            massively.getLogger().info("Disabling the handlers...");
            this.forEach(handler -> {
                massively.getLogger().info("Disabling the handler with name '" + handler.getName() + "'.");
                handler.onDisable();
            });
            handlers.clear();
        }

        @Override
        public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
            for (Handler handler : this) {
                if (handler.onCommand(sender, command, label, args)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
            for (Handler handler : this) {
                List<String> returned;
                if ((returned = handler.onTabComplete(sender, command, alias, args)) != null) {
                    return returned;
                }
            }
            return null;
        }

        @Override
        public boolean allowFight(Location location) {
            if (!location.getWorld().getPVP()) {
                return false;
            }
            for (Handler handler : this) {
                if (!handler.allowFight(location)) {
                    return false;
                }
            }
            return true;
        }

        @Override
        public boolean allowFight(Player player) {
            if (!player.getWorld().getPVP()) {
                return false;
            }
            for (Handler handler : this) {
                if (!handler.allowFight(player)) {
                    return false;
                }
            }
            return true;
        }

        public boolean registerHandler(Handler handler) {
            if (!handlers.containsKey(Objects.requireNonNull(handler).getName())) {
                handlers.put(handler.getName(), handler);
                return true;
            }
            return false;
        }

        @Override
        public Iterator<Handler> iterator() {
            return handlers.values().iterator();
        }
    }

    @SuppressWarnings("unused")
    public static abstract class Handler {
        public void onLoad() { }

        public void onEnable() { }

        public void onDisable() { }

        public abstract String getName();

        public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
            return false;
        }

        public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
            return null;
        }

        public boolean allowFight(Location location) {
            return true;
        }

        public boolean allowFight(Player player) {
            return true;
        }
    }
}
