/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.massively;

import com.sk89q.intake.Command;
import com.sk89q.intake.CommandException;
import com.sk89q.intake.Require;
import com.sk89q.intake.parametric.annotation.Optional;
import com.sk89q.intake.util.auth.AuthorizationException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import me.nikosgram.massively.api.chat.MessageColor;
import me.nikosgram.massively.api.entity.CommandExecutor;
import me.nikosgram.massively.api.entity.Mobile;

@SuppressWarnings("unused")
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class MassivelyCommands {
    @Command(aliases = "rank", desc = "Check your rank or change your rank.")
    @Require("massively.rank")
    public void rank(CommandExecutor executor, @Optional Mobile mobile) throws AuthorizationException, CommandException {
        Mobile target;
        if (mobile == null) {
            if (executor instanceof Mobile) {
                target = (Mobile) executor;
            } else {
                throw new CommandException("I don't know who are you.");
            }
        } else {
            if (executor.hasPermission("massively.rank.other")) {
                target = mobile;
            } else {
                throw new AuthorizationException();
            }
        }
        if (target.getRank().isPresent()) {
            executor.sendMessage(
                    MessageColor.YELLOW + target.getName() + MessageColor.GRAY + "'s rank is " +
                            MessageColor.GREEN + target.getRank().get().getName() + MessageColor.GRAY + "."
            );
        } else {
            executor.sendMessage(MessageColor.YELLOW + target.getName() + MessageColor.GRAY + " has not select rank.");
        }
    }

    @Command(aliases = "level", desc = "Check your levels")
    @Require("massively.level")
    public void level(CommandExecutor executor, @Optional Mobile mobile) throws AuthorizationException, CommandException {
        Mobile target;
        if (mobile == null) {
            if (executor instanceof Mobile) {
                target = (Mobile) executor;
            } else {
                throw new CommandException("I don't know who are you.");
            }
        } else {
            if (executor.hasPermission("massively.level.other")) {
                target = mobile;
            } else {
                throw new AuthorizationException();
            }
        }
        executor.sendMessage(
                MessageColor.YELLOW + target.getName() + MessageColor.GRAY + "'s level is " +
                        MessageColor.GREEN + target.getExperiencesData().getLevel() + MessageColor.GRAY + " and has " +
                        MessageColor.GREEN + target.getExperiencesData().getExperiences() + MessageColor.GRAY + " experiences."
        );
    }

    @Command(aliases = "mana", desc = "Check your mana")
    @Require("massively.mana")
    public void mana(CommandExecutor executor, @Optional Mobile mobile) throws AuthorizationException, CommandException {
        Mobile target;
        if (mobile == null) {
            if (executor instanceof Mobile) {
                target = (Mobile) executor;
            } else {
                throw new CommandException("I don't know who are you.");
            }
        } else {
            if (executor.hasPermission("massively.mana.other")) {
                target = mobile;
            } else {
                throw new AuthorizationException();
            }
        }
        executor.sendMessage(
                MessageColor.YELLOW + target.getName() + MessageColor.GRAY + "'s mana is " +
                        MessageColor.GREEN + target.getManaData().getMana() + MessageColor.GRAY + '/' +
                        MessageColor.RED + target.getManaData().getMaxMana() + MessageColor.GRAY + "."
        );
    }

    @Command(aliases = "reset", desc = "Reset your rank")
    @Require("massively.reset")
    public void reset(CommandExecutor executor, @Optional Mobile mobile) throws AuthorizationException, CommandException {
        Mobile target;
        if (mobile == null) {
            if (executor instanceof Mobile) {
                target = (Mobile) executor;
            } else {
                throw new CommandException("I don't know who are you.");
            }
        } else {
            if (executor.hasPermission("massively.reset.other")) {
                target = mobile;
            } else {
                throw new AuthorizationException();
            }
        }
        target.resetMobile();
        target.sendMessage(MessageColor.GRAY + "Your profile resetted.");
    }
}
