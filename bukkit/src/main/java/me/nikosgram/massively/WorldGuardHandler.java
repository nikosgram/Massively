/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.massively;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class WorldGuardHandler extends MassivelyBukkit.Handler implements Listener {
    @Getter
    private final String name = "WorldGuard";
    private final MassivelyBukkit massively;

    @Override
    public boolean allowFight(Location location) {
        StateFlag.State allowPVP = WorldGuardPlugin.inst().getRegionManager(location.getWorld()).getApplicableRegions(location).queryState(null, DefaultFlag.PVP);
        return allowPVP == null || allowPVP.equals(StateFlag.State.ALLOW);
    }

    @Override
    public boolean allowFight(Player player) {
        return allowFight(player.getLocation());
    }
}
