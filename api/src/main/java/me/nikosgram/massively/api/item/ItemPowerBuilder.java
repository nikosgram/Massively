/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.massively.api.item;

import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
@RequiredArgsConstructor
public class ItemPowerBuilder {
    private int id = 0;
    private byte data = 0;
    private String name = "";
    private String rank = "";
    private double mana = 0.0;
    private int level;
    private Map<String, Object> values = new HashMap<>();

    public ItemPowerBuilder id(int id) {
        this.id = id;
        return this;
    }

    public ItemPowerBuilder data(byte data) {
        this.data = data;
        return this;
    }

    public ItemPowerBuilder name(String name) {
        this.name = name;
        return this;
    }

    public ItemPowerBuilder rank(String rank) {
        this.rank = rank;
        return this;
    }

    public ItemPowerBuilder mana(double mana) {
        this.mana = mana;
        return this;
    }

    public ItemPowerBuilder level(int level) {
        this.level = level;
        return this;
    }

    public ItemPowerBuilder value(String key, Object value) {
        values.put(key, value);
        return this;
    }

    public ItemPower create() {
        return new ItemPower() {
            @Override
            public int getId() {
                return id;
            }

            @Override
            public byte getData() {
                return data;
            }

            @Override
            public String getName() {
                return name;
            }

            @Override
            public String getRank() {
                return rank;
            }

            @Override
            public double getMana() {
                return mana;
            }

            @Override
            public int getLevel() {
                return level;
            }

            @Override
            public Map<String, Object> getValues() {
                return values;
            }
        };
    }
}
