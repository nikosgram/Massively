/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.massively;

import lombok.Getter;
import me.nikosgram.massively.api.rank.Rank;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings("unsued")
public class HumanRank implements Rank, Listener {
    private final MassivelyBukkit massively;
    @Getter
    private final String name = "Human";
    @Getter
    private final String id = "human";
    @Getter
    private final Collection<Rank> parents = new ArrayList<>();
    @Getter
    private final double health = 20;
    @Getter
    private final double startMana = 0;
    @Getter
    private final double appendManaPerLevel = 0;
    @Getter
    private final double appendManaPerRefresh = 0;
    private boolean registered = false;

    protected HumanRank(MassivelyBukkit massively) {
        this.massively = massively;
    }

    @Override
    public void registerEvents() {
        if (registered) {
            throw new RuntimeException();
        }
        massively.getServer().getPluginManager().registerEvents(this, massively);
        registered = true;
    }
}
