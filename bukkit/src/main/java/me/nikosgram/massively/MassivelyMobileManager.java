/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.massively;

import com.sk89q.intake.argument.ArgumentException;
import com.sk89q.intake.argument.ArgumentParseException;
import com.sk89q.intake.argument.CommandArgs;
import com.sk89q.intake.parametric.ProvisionException;
import me.nikosgram.massively.api.MobileManager;
import me.nikosgram.massively.api.chat.MessageColor;
import me.nikosgram.massively.api.entity.Mobile;
import me.nikosgram.massively.api.rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import javax.annotation.Nullable;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@SuppressWarnings({"unused", "deprecation"})
public class MassivelyMobileManager implements MobileManager, Listener {
    protected final Map<UUID, Mobile> mobiles = new HashMap<>();
    //    protected final Map<UUID, Rank> ranks = new HashMap<>();
    protected final Map<UUID, Integer> tasks = new HashMap<>();
    private final MassivelyBukkit massively;

    protected MassivelyMobileManager(MassivelyBukkit massively) {
        this.massively = Objects.requireNonNull(massively);
        Bukkit.getPluginManager().registerEvents(this, massively);
        massively.connector.createTable("_experiences", "id int identity(1,1) primary key", "uuid varchar(36)", "experiences double");
        massively.connector.createTable("_mana", "id int identity(1,1) primary key", "uuid varchar(36)", "mana double");
        massively.connector.createTable("_rank", "id int identity(1,1) primary key", "uuid varchar(36)", "rank text");
    }

    @Override
    public Optional<Mobile> getMobile(UUID target) {
        if (mobiles.containsKey(target)) {
            return Optional.of(mobiles.get(target));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Mobile> getMobile(String target) {
        OfflinePlayer player = Bukkit.getOfflinePlayer(target);
        return Optional.ofNullable(player != null ? getMobile(player.getUniqueId()).orElse(null) : null);
    }

    @Override
    public boolean contains(String target) {
        OfflinePlayer player = Bukkit.getOfflinePlayer(target);
        return player != null && contains(player.getUniqueId());
    }

    @Override
    public boolean contains(UUID target) {
        return mobiles.containsKey(target);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void checkExperiences(PlayerJoinEvent event) {
        UUID uuid = event.getPlayer().getUniqueId();
        if (!massively.connector.exists("_experiences", "uuid", uuid.toString())) {
            Map<String, String> values = new HashMap<>();
            values.put("uuid", uuid.toString());
            values.put("experiences", "0.0");
            massively.connector.insert("_experiences", values);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void checkMana(PlayerJoinEvent event) {
        UUID uuid = event.getPlayer().getUniqueId();
        if (!massively.connector.exists("_mana", "uuid", uuid.toString())) {
            Map<String, String> values = new HashMap<>();
            values.put("uuid", uuid.toString());
            values.put("mana", "0.0");
            massively.connector.insert("_mana", values);
        }
    }

    @EventHandler
    public void appendHuman(PlayerJoinEvent event) {
        UUID uuid = event.getPlayer().getUniqueId();
        mobiles.put(uuid, new MassivelyMobile(massively, event.getPlayer()));
        tasks.put(uuid, Bukkit.getScheduler().scheduleSyncRepeatingTask(massively, () -> {
            Optional<Mobile> mobile = getMobile(uuid);
            if (mobile.isPresent()) {
                Optional<Rank> rank = mobile.get().getRank();
                if (rank.isPresent()) {
                    mobile.get().getManaData().appendMana(rank.get().getAppendManaPerRefresh() + (2.5 * (mobile.get().getExperiencesData().getLevel() - 1)));
                }
            }
        }, 50, 50));
        if (!mobiles.get(uuid).getRank().isPresent()) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(massively, () -> {
                Inventory inventory = Bukkit.createInventory(null, 27, MessageColor.GREEN + "Select your rank.");
                {
                    ItemStack stack = new ItemStack(Material.BLAZE_ROD);
                    ItemMeta meta = stack.getItemMeta();
                    meta.setDisplayName(MessageColor.DARK_PURPLE + "Mage");
                    stack.setItemMeta(meta);
                    inventory.setItem(11, stack);
                }
                {
                    ItemStack stack = new ItemStack(Material.DIAMOND);
                    ItemMeta meta = stack.getItemMeta();
                    meta.setDisplayName(MessageColor.DARK_AQUA + "Human");
                    stack.setItemMeta(meta);
                    inventory.setItem(13, stack);
                }
                {
                    ItemStack stack = new ItemStack(Material.BOW);
                    ItemMeta meta = stack.getItemMeta();
                    meta.setDisplayName(MessageColor.DARK_GREEN + "Archer");
                    stack.setItemMeta(meta);
                    inventory.setItem(15, stack);
                }
                event.getPlayer().openInventory(inventory);
            }, 10);
        }
    }

    @EventHandler
    public void onClickInventory(InventoryClickEvent event) {
        if (event.getInventory().getName().equals(MessageColor.GREEN + "Select your rank.")) {
            event.setCancelled(true);
            Mobile mobile = getMobile(event.getWhoClicked().getUniqueId()).get();
            boolean close = false;
            switch (event.getSlot()) {
                case 11:
                    mobile.changeRank(massively.rankManager.getRank("mage").get());
                    close = true;
                    break;
                case 13:
                    mobile.changeRank(massively.rankManager.getRank("human").get());
                    close = true;
                    break;
                case 15:
                    mobile.changeRank(massively.rankManager.getRank("archer").get());
                    close = true;
                    break;
            }
            if (close) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(massively, () -> event.getWhoClicked().closeInventory());
            }
        }
    }

    @EventHandler
    public void removeHuman(PlayerQuitEvent event) {
        UUID uuid = event.getPlayer().getUniqueId();
        if (mobiles.containsKey(uuid)) {
            mobiles.remove(uuid);
        }
        if (tasks.containsKey(uuid)) {
            Bukkit.getScheduler().cancelTask(tasks.get(uuid));
            tasks.remove(uuid);
        }
    }

    @EventHandler
    public void onKillEntity(EntityDeathEvent event) {
        if (event.getEntity().getKiller() != null) {
            double experiences = getExperiences(event.getEntity());
            massively.getKillers(event.getEntity()).forEach(player -> {
                Mobile mobile = massively.mobileManager.getMobile(player.getUniqueId()).get();
                if (mobile.hasRank()) {
                    mobile.getExperiencesData().appendExperiences(experiences);
                }
            });
        }
    }

    private double getExperiences(Entity entity) {
        switch (entity.getType()) {
            case PLAYER:
                return 55;
            default:
                EntityCategory category = EntityCategory.getCategory(entity.getType());
                switch (category) {
                    case Boss:
                        return 78.95;
                    case Unused:
                        return 38.5;
                    case Utility:
                        return 5;
                    case Passive:
                    case Tamable:
                        return 2.5;
                    case Hostile:
                        return 20.25;
                    case Neutral:
                        return 25.25;
                }
        }
        return 0;
    }

    @Override
    public Iterator<Mobile> iterator() {
        return mobiles.values().iterator();
    }

    @Override
    public boolean isProvided() {
        return false;
    }

    @Nullable
    @Override
    public Mobile get(CommandArgs arguments, List<? extends Annotation> modifiers) throws ArgumentException, ProvisionException {
        String name = arguments.next();
        Optional<Mobile> mobile = getMobile(name);
        if (mobile.isPresent()) {
            return mobile.get();
        } else {
            throw new ArgumentParseException(String.format("I can't find the Player with name '%s'.", name));
        }
    }

    @Override
    public List<String> getSuggestions(String prefix) {
        List<String> returned = new ArrayList<>();
        this.forEach(mobile -> {
            if (mobile.getName().startsWith(prefix)) {
                returned.add(mobile.getName());
            }
        });
        return returned;
    }
}
